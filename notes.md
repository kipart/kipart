# Development notes

- kipart manages stores
  - `/parts`
  - `/schematics`
  - `/footprints`
  - `/models`
- A part is just symlinks to some sets in the store

```
my_part/
  comptonen.json
  base_component.json -> /parts/alksjdalksjdlkad
  symbol.lib -> /schematic/asldkjaslkdjads
  footprint.pretty -> /footprint/sdfjlksjfdlksdjf
  3dmodel.wrml -> /models/kjhsfkjsdhfkjshfkjsh
```

## Base component file contains

```json
{
  "type": "resistor",
  "spice": "R",
  "optionals": [ "name", "serial_number", "datasheets" ],
  "required": [ "manufacturer", "part_id" ],
}
```

## Project structure

- A library that manages part caches
- A CLI that uses the library
- A KiCAD plugin that uses the library

## KiPart library

Similar to `nixpkgs`. `kiparts` contains all parts.

```
kiparts/
  store/
    base_components/
    schematics/
    footprints/
    models/
  components/
    st/
      stm32_foobar/
        component.json 
        <symlinks here>
    ge/
      generic_r_0602/
        component.json 
```

- Project cache (`.kipart_cache`)
- User cache (`~/.cache/kipart`)
- system cache (`/var/lib/kipart`)

Parts that are added to the repository

- `requirements.json`
- `requirements.lock`

Some environment variables that can be set

- `kipart_repo_source` => gitlab.com/kipart/library | ~/dev/kipart_library
- 

## kipart.org web search

REST API for searching part library more quickly.

WIP

## Roadmap

- Read kicad stdlib to metadata
  - Generate base components and components files
  - Organise everything into part stores
- Define store layouts
  - `/store/base/mcp4017.json`
  - `/store/symbols/mcp4017.lib`
  - `/store/footprints/sop16.pretty`
  - `/store/models/sop16_std.wrml`
- Write abstraction for store interactions
- 
