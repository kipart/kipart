[![pipeline status](https://gitlab.com/KiPART/KiPART/badges/master/pipeline.svg)](https://gitlab.com/KiPART/KiPART/commits/master)
[![coverage report](https://gitlab.com/KiPART/KiPART/badges/master/coverage.svg)](https://gitlab.com/KiPART/KiPART/commits/master)

```
 _____  _  _____  _____  _____  _____
|  |  ||_||  _  ||  _  || __  ||_   _|
|    -|| ||   __||     ||    -|  | |
|__|__||_||__|   |__|__||__|__|  |_|
```

# KiPART

Main application
