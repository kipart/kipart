FROM alpine:latest

LABEL version="0.0.1" \
      description="Docker image with KiPART used for KiPART/library CI jobs" \
      maintainer="mail@kipart.rocks"

COPY ./kipart /usr/local/bin/kipart

CMD ["kipart"]
